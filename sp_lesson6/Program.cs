﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace sp_lesson6
{
    class Program
    {
        delegate int MyAsyncDelegate(int x, int y);

        static void Main(string[] args)
        {
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} started work");

            var asyncDelegate = new MyAsyncDelegate(Calculate);

            //Обычный путь
            //var result = asyncDelegate.Invoke(10, 15);
            //Console.WriteLine("Sum result is: " + result);

            // Асинхронный вариант 1
            //var asyncResult = asyncDelegate.BeginInvoke(10, 15, null, null);
            //while (!asyncResult.IsCompleted)
            //{
            //    Console.WriteLine("Wait, work in progress");
            //    Thread.Sleep(800);
            //}
            //var result = asyncDelegate.EndInvoke(asyncResult);
            //Console.WriteLine("Sum result is: " + result);


            // Асинхронный вариант 1
            asyncDelegate.BeginInvoke(10, 15, new AsyncCallback(CalculateCallback), "Какое-то значение, можно не строкой");
           
            Console.ReadLine();
        }

        // Метод для Асинхронного варианта 2
        private static void CalculateCallback(IAsyncResult asyncResult)
        {
            var data = asyncResult.AsyncState as string; //До информация, которую нужно передать в CallBack из вне
            var asyncDelegate = (asyncResult as AsyncResult).AsyncDelegate as MyAsyncDelegate;
            var result = asyncDelegate.EndInvoke(asyncResult);
            Console.WriteLine("Sum result is: " + result);
        }

        static int Calculate(int firstNumber, int secondNumber)
        {
            Console.WriteLine($"Thread {Thread.CurrentThread.ManagedThreadId} started work");
            Thread.Sleep(5000);
            return firstNumber+secondNumber;
        }
    }
}
